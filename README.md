# SCC0251 Digital Image Processing

## Assignment 2: Image Enhancement and Filtering

**Author:**

- Alexandre Norcia Medeiros

**Folders and Files:**

- [Images](./images): contains images used in the jupyter to demonstrate the filters effects.
- [Python code](./submission) : contains the code submitted for grading in the run.codes system.
- [PDF File](./dip_t02_image_enhancement_filter-vignetting.pdf): the assignment description.
- [Notebook](./t2-image-filtering-examples.ipynb): a python notebook exemplifying the code functions and image results.
