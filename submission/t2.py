""" 
    Alexandre Norcia Medeiros - nUSP: 10295583
    ICMC - USP, 2020 - 1o semestre

    SCC0251 - Digital Image Processing.
    Assignment 2:  Image Enhancement and Filtering

    GIT repository url: https://gitlab.com/alexandrenmedeiros/scc0251-pdi-assigment-2
"""

import numpy as np
import imageio


def normalize_image(img):
    """ returns a image with its values normalized between [0:255] """
    min_val = np.amin(img)
    max_val = np.amax(img)

    return ( ((img - min_val) * 255) / (max_val - min_val))


def convolution(img, f):
    """ Applies a convolution operation on an image given a filter """

    f_center = int((f.shape[1] - 1) / 2) # shift
    f_flip = np.flip(np.flip(f, 0), 1).astype(np.float) # flip filter to make cross-correlation

    out_img = np.array(img, copy=True)
    img = np.pad(img, pad_width = f_center, mode='constant', constant_values = 0)
    N, M = img.shape
   
    # applies the convolution
    for x in range(f_center, N - f_center):
        for y in range(f_center, M - f_center):
            # get the frame which the filter uses to calculate the new pixel value
            frame = img[x - f_center: x+f_center+1, y-f_center: y+f_center+1]
            
            # alter the image
            out_img[x-f_center][y-f_center] = np.sum(np.multiply(frame, f_flip))

    return out_img


def gaussian_2d(x, y, sigma):
    """ Compute a 2 dimension gaussian function for the given parameters  """

    return  ( 1 / (2 * np.pi  * sigma**2)) * np.exp(-1* (x**2 + y**2) / (2* sigma**2))


def bilateral_filter(img, n, sigma_s, sigma_r):
    """ Applies a bilateral filter in a image """

    img = img.astype(np.float) # conversion to avoid infomation loss due overflow

    # creates the spatial component
    sp_kernel = np.zeros((n,n))
    f_center = int((n-1)/2) # shift
    for i in range(0, n):
        for j in range(0, n):
            sp_kernel[i][j] = gaussian_2d(i - f_center, j - f_center, sigma_s)

    out_img = np.array(img, copy=True)
    img = np.pad(img, pad_width = f_center, mode='constant', constant_values = 0)
    N, M = img.shape
    # applies the convolution computing the range component for each sub frame
    for x in range(f_center, N - f_center):
        for y in range(f_center, M - f_center):
            # get the frame which the filter uses to calculate the new pixel value
            frame = img[x - f_center : x+f_center +1, y-f_center : y+f_center +1]
            p_i = frame[f_center][f_center] # original value of the pixel to be altered
            p_f = 0 # final value of the pixel
            wp = 0 # normalization factor

            for i in range(0, n):
                for j in range(0, n):
                    rng_kernel_element = gaussian_2d( frame[i][j] - p_i, 0, sigma_r)
                    p_f += frame[i][j] * sp_kernel[i][j] * rng_kernel_element
                    wp += sp_kernel[i][j] * rng_kernel_element

            # alter the image
            out_img[x-f_center][y-f_center] = p_f / wp

    return out_img.astype(np.uint8)


def laplacian_kernel(d = 1):
    """ returns one of two discrete approximation of the laplacian kernel """
    if (d == 1):
        r = np.array([
            [0, -1, 0], 
            [-1, 4, -1],
            [0, -1, 0]])
    else:
        r = np.array([
            [-1, -1, -1],
            [-1,  8, -1],
            [-1, -1, -1]])

    return r


def laplacian_filter(img, c, kernel_t):
    """ Applies a laplacian filter in a image using one of two kernels """

    img = img.astype(np.float)# conversion to avoid infomation loss due overflow

    kernel = laplacian_kernel(kernel_t)

    fil_img = convolution(img, kernel) # filtered image
    fil_img = normalize_image(fil_img) # normalize the image in the interval [0:255]
    img = (c * fil_img) + img          # applie the filtered image multiplyed by a constant in the original image
    img = normalize_image(img)         # normalize the final image

    return img.astype(np.uint8)


def vignette_filter(img, sigma_row, sigma_col):
    """ Applies a vignette filter in a image  """

    img = img.astype(np.float) # conversion to avoid infomation loss due overflow
    N, M = img.shape

    row_center = int((N-1)/2)
    col_center = int((M-1)/2)
    # creates row kernel
    row_kernel = np.zeros((N,1))
    for i in range(0,N):
        row_kernel[i][0] = gaussian_2d( i - row_center, 0, sigma_row)
    # creates col
    col_kernel = np.zeros((1, M))
    for i in range(0, M):
        col_kernel[0][i] = gaussian_2d(i - col_center, 0, sigma_col)

    # computes gaussian matrix and applies to image
    out_image = np.multiply( (row_kernel * col_kernel), img)
    out_image = normalize_image(out_image)

    return out_image.astype(np.uint8)

def root_sqr_error(img1, img2):
    """ Calculates the root squared error between two images. """

    dif = img1.astype(np.float) - img2.astype(np.float)

    return np.sqrt(np.sum(np.power(dif, 2)))


# def main():

# read inputs
img_name = str(input()).rstrip()
method = int(input())
save = int(input())

# reads image from '.' directory
img = imageio.imread(img_name)

# verifies which filtering method is to be applied
if (method == 1):
    n = int(input())
    sigma_s = float(input())
    sigma_r = float(input())
    out_img = bilateral_filter(img, n, sigma_s, sigma_r)

elif (method == 2):
    c = float(input())
    kernel_t = int(input())
    out_img = laplacian_filter(img, c, kernel_t)

elif (method == 3):
    sigma_row = float(input())
    sigma_col = float(input())
    out_img = vignette_filter(img, sigma_row, sigma_col)

# verifies if the modified image needs to be saved
if (save):
    imageio.imwrite('output_image.png', out_img)


# calculates and print the root esquared error
rse = root_sqr_error(img, out_img)
print('%.4f' % rse)
